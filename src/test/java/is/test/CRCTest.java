package is.test;

import is.CRC;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

/**
 * Created by dapqa on 29.03.2016.
 * Эталонные значения вычислены на сайте https://www.ghsi.de/CRC/
 * Кодировка сообщений, используемая при проверке - ASCII.
 * Используемые полиномы (указаны в классе CRC):
 *      CRC-4: 10011
 *      CRC-5: 110101
 *      CRC-8: 111010101
 *      CRC-12: 1100000001111
 */
public class CRCTest {

    private static String[] TEST_STRINGS = {
            "Test",
            "123456789",
            "I love informational security",
            "VyatSU",
            "Roses are red. Violets are blue."
    };

    private static int[] CRC4_EXPECTED = {
            0xA, 0xE, 0x1, 0x6, 0x2
    };

    private static int[] CRC5_EXPECTED = {
            0x14, 0x1C, 0x2, 0xC, 0x17
    };

    private static int[] CRC8_EXPECTED = {
            0x3C, 0xBC, 0x4A, 0x99, 0xDB
    };

    private static int[] CRC12_EXPECTED = {
            0xB5E, 0xF5B, 0x83A, 0x3F3, 0x518
    };

    @Test
    public void testCRC4() {
        for (int i = 0; i < TEST_STRINGS.length; i++) {
            int crc = CRC.crc4(TEST_STRINGS[i].getBytes(StandardCharsets.US_ASCII));
            assertEquals(crc, CRC4_EXPECTED[i]);
        }
    }

    @Test
    public void testCRC5() {
        for (int i = 0; i < TEST_STRINGS.length; i++) {
            int crc = CRC.crc5(TEST_STRINGS[i].getBytes(StandardCharsets.US_ASCII));
            assertEquals(crc, CRC5_EXPECTED[i]);
        }
    }

    @Test
    public void testCRC8() {
        for (int i = 0; i < TEST_STRINGS.length; i++) {
            int crc = CRC.crc8(TEST_STRINGS[i].getBytes(StandardCharsets.US_ASCII));
            assertEquals(crc, CRC8_EXPECTED[i]);
        }
    }

    @Test
    public void testCRC12() {
        for (int i = 0; i < TEST_STRINGS.length; i++) {
            int crc = CRC.crc12(TEST_STRINGS[i].getBytes(StandardCharsets.US_ASCII));
            assertEquals(crc, CRC12_EXPECTED[i]);
        }
    }

}
