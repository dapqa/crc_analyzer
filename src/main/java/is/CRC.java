package is;

/**
 * Created by Dapqa on 23.03.2016.
 * Используется для расчета CRC-4, CRC-5, CRC-8, CRC-12.
 * При расчете CRC-N считается, как будто идет работа с N-разрядным регистром.
 * Для всех возмодных значений от 0 до 2^N - 1 предрасчитаны значения CRC.
 */
public class CRC {

    private static byte[] CRC4_TABLE;
    private static final int CRC4_POLY = 0x3;

    private static byte[] CRC5_TABLE;
    private static final int CRC5_POLY = 0x15;

    private static byte[] CRC8_TABLE;
    private static final int CRC8_POLY = 0xD5;

    private static short[] CRC12_TABLE;
    private static final int CRC12_POLY = 0x80F;

    static {
        CRC4_TABLE = calculateByteTable(4, CRC4_POLY);
        CRC5_TABLE = calculateByteTable(5, CRC5_POLY);
        CRC8_TABLE = calculateByteTable(8, CRC8_POLY);
        CRC12_TABLE = calculateShortTable(12, CRC12_POLY);
    }

    public static int crc4(byte[] data) {
        int crc = 0;
        for (byte b : data) {
            // С обеими половинками байта идет работа, как с 4-разрядным регистром
            byte high = (byte) ((b & 0xF0) >> 4);
            byte low = (byte) (b & 0xF);

            crc ^= high;
            crc = CRC4_TABLE[crc & 0xF];

            crc ^= low;
            crc = CRC4_TABLE[crc & 0xF];
        }
        return crc & 0xF;
    }

    public static int crc5(byte[] data) {
        int crc = 0;
        // Массив байт разбивается на группы по 5 бит, и дальше работа идет
        // как с 5-битным регистром
        byte[] converted = crc5convert(data);
        for (byte b : converted) {
            crc = CRC5_TABLE[(crc ^ b) & 0x1F];
        }
        return crc & 0x1F;
    }

    public static int crc8(byte[] data) {
        int crc = 0;
        // Самый простой случай (8-битный регистр)
        for (byte b : data) {
            crc = CRC8_TABLE[(crc ^ b) & 0xFF];
        }
        return crc & 0xFF;
    }

    public static int crc12(byte[] data) {
        int crc = 0;
        // Дополнение массива, чтобы количество байт было кратно трем (дополнение идет со старших байт)
        if (data.length % 3 != 0) {
            byte[] temp = new byte[data.length + 3 - data.length % 3];
            System.arraycopy(data, 0, temp, 3 - data.length % 3, data.length);
            data = temp;
        }

        for (int i = 0; i < data.length / 3; i++) {
            byte b0 = data[i * 3];
            byte b1 = data[i * 3 + 1];
            byte b2 = data[i * 3 + 2];

            // Три байт рассматриваются как два 12-битных регистра
            short high = (short) (((short)b0) << 4 | (b1 & 0xF0) >> 4);
            short low = (short) (((short)b2) | ((short)(b1 & 0x0F) << 8));

            crc ^= high;
            crc = CRC12_TABLE[crc & 0xFFF];

            crc ^= low;
            crc = CRC12_TABLE[crc & 0xFFF];
        }

        return crc & 0xFFF;
    }

    /**
     * Возвращает таблицу для быстрого расчета CRC-N
     * @param n CRC-N, n <= 8
     * @param poly Полином степени N
     * @return Таблица для расчета
     */
    private static byte[] calculateByteTable(int n, int poly) {
        byte[] res = new byte[1 << n];

        for (int i = 0; i < 1 << n; i++) {
            byte value = (byte)i;
            for (byte j = 0; j < n; j++) {
                value = (byte)((value & (1 << (n - 1))) != 0 ? ((value << 1) ^ poly) : value << 1);
            }
            res[i] = value;
        }

        return res;
    }

    /**
     * Возвращает таблицу для быстрого расчета CRC-N
     * @param n CRC-N, n <= 16
     * @param poly Полином степени N
     * @return Таблица для расчета
     */
    private static short[] calculateShortTable(int n, int poly) {
        short[] res = new short[1 << n];

        for (int i = 0; i < 1 << n; i++) {
            short value = (short)i;
            for (byte j = 0; j < n; j++) {
                value = (short)((value & (1 << (n - 1))) != 0 ? ((value << 1) ^ poly) : value << 1);
            }
            res[i] = value;
        }

        return res;
    }

    /**
     * Разбивает массив байт на группы по 5 бит. Недостающие биты записываются в старшей части.
     * @param data Массив байт (например, { 0b10011101, 0b00101010 }
     * @return Массив групп по 5 бит (для примера - { 0b000_00001, 0b000_00111, 0b000_01001, 0b000_01010 })
     */
    private static byte[] crc5convert(byte[] data) {
        int size = data.length * 8 / 5 + (data.length * 8 % 5 != 0 ? 1 : 0);
        byte[] res = new byte[size];

        int resPos = size - 1;
        int cur = data.length - 1;
        int state = 0;
        byte el;
        while (resPos >= 0) {
            switch (state) {
                case 0:
                    // 00000000_00011111
                    el = (byte) (data[cur] & 0x1F);
                    break;
                case 1:
                    // 00000011_11100000
                    el = (byte) (data[cur] >> 5);
                    cur--;
                    if (cur >= 0) el |= (data[cur] & 0x03) << 3;
                    break;
                case 2:
                    // 00000000_01111100
                    el = (byte) ((data[cur] >> 2) & 0x1F);
                    break;
                case 3:
                    // 00001111_10000000
                    el = (byte) (data[cur] >> 7);
                    cur--;
                    if (cur >= 0) el |= (data[cur] & 0x0F) << 1;
                    break;
                case 4:
                    // 00000001_11110000
                    el = (byte) (data[cur] >> 4);
                    cur--;
                    if (cur >= 0) el |= (data[cur] & 0x01) << 4;
                    break;
                case 5:
                    // 00000000_00111110
                    el = (byte) ((data[cur] >> 1) & 0x1F);
                    break;
                case 6:
                    // 00000111_11000000
                    el = (byte) (data[cur] >> 6);
                    cur--;
                    if (cur >= 0) el |= (data[cur] & 0x07) << 2;
                    break;
                case 7:
                    // 00000000_11111000
                    el = (byte) ((data[cur] >> 3) & 0x1F);
                    cur--;
                    break;
                default:
                    el = 0;
            }

            res[resPos] = el;
            state = (state + 1) % 8;
            resPos--;
        }

        return res;
    }

}
