package is;

import is.controller.MainFormController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * Created by dapqa on 21.03.2016.
 */
public class CRCAnalyzerApp extends Application {

    /**
     * CalculationResult(X) описывает результат анализа CRC-N
     */
    public class CalculationResult {
        // counts[i] = Количество слов, которые имеют хэш i
        public int[] counts;

        // Хэш, имеющий максимальное количество коллизий
        // Если их несколько, то используется первый полученный
        public int maxConflictingCRC;

        // Слова, имеющие хэш maxConflictingCRC
        public String[] wordsWithMaxConflictingCRC;

        @SuppressWarnings("unused")
        private CalculationResult() {}

        public CalculationResult(int n) {
            counts = new int[1 << n];
        }
    }

    private File inputFile;

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResourceAsStream("/fxml/MainForm.fxml"));

        MainFormController mainFormController = loader.getController();
        mainFormController.setCrcAnalyzerApp(this);
        mainFormController.setStage(primaryStage);

        primaryStage.setTitle("Анализ работы CRC");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.sizeToScene(); // Скорее всего, при использовании JDK7 будут лишние поля в каждом окне
        primaryStage.show();
    }

    /**
     * Вычисляет массив CalculationResult согласно заданию
     * @return Массив из 4-х CalculationResult для CRC-4, CRC-5, CRC-8 и CRC-12 соответственно
     * @throws IOException При ошибке чтения или парсинга файла
     */
    public CalculationResult[] calculate() throws IOException {
        if (inputFile == null) throw new IOException("Файл не указан.");

        String content = new String(Files.readAllBytes(inputFile.toPath()), Charset.forName("CP1251")).trim();
        String[] words = content.split("[\\p{Punct}\\s]+");
        String[] uniqueWords = Arrays.stream(words).distinct().toArray(String[]::new);

        if (uniqueWords.length == 0) {
            if (content.isEmpty()) {
                throw new IOException("Файл пустой");
            } else {
                throw new IOException("Файл либо имеет кодировку, отличную от cp1251, либо не содержит слов");
            }
        }

        CalculationResult[] res = {
                new CalculationResult(4),
                new CalculationResult(5),
                new CalculationResult(8),
                new CalculationResult(12)
        };
        List<Function<byte[], Integer>> crcList = Arrays.asList(CRC::crc4, CRC::crc5, CRC::crc8, CRC::crc12);

        // crcValues[i][j] содержит значение CRC для i-го уникального слова, полученное методом crcList.get(j)
        int[][] crcValues = new int[uniqueWords.length][res.length];
        // max[i] содержит максимальное количество слов, получивщих одинаковый хэш методом crcList.get(i)
        // Такой хэш хранится в res[i].maxConflictingCRC
        int[] max = new int[res.length];

        // Заполнение crcValues, max, и res (кроме списка слов)
        for (int i = 0; i < uniqueWords.length; i++) {
            byte[] data = uniqueWords[i].getBytes();

            for (int j = 0; j < res.length; j++) {
                int crc = crcList.get(j).apply(data);

                res[j].counts[crc]++;
                if (max[j] < res[j].counts[crc]) {
                    max[j] = res[j].counts[crc];
                    res[j].maxConflictingCRC = crc;
                }

                crcValues[i][j] = crc;
            }
        }

        // Заполнение списка слов для каждого res[i]
        for (int i = 0; i < res.length; i++) {
            res[i].wordsWithMaxConflictingCRC = new String[max[i]];
            int pos = 0, wordsPos = 0;
            while (wordsPos < max[i]) {
                if (crcValues[pos][i] == res[i].maxConflictingCRC) {
                    res[i].wordsWithMaxConflictingCRC[wordsPos] = uniqueWords[pos];
                    wordsPos++;
                }
                pos++;
            }
        }

        return res;
    }

    public void setInputFile(File inputFile) {
        this.inputFile = inputFile;
    }

}
