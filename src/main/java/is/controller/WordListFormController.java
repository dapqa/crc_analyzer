package is.controller;

import com.sun.javafx.collections.ObservableListWrapper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.Observable;

/**
 * Created by Dapqa on 21.03.2016.
 */
public class WordListFormController {

    public ListView<String> wordsListView;

    private Stage stage;

    public void closeForm(ActionEvent actionEvent) {
        stage.close();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setWords(String[] words) {
        if (words != null && words.length >= 0) {
            wordsListView.setItems(new ObservableListWrapper<>(Arrays.asList(words)));
        } else {
            wordsListView.setItems(null);
        }
    }
}
