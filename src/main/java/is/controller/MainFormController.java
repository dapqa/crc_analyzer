package is.controller;

import com.sun.javafx.collections.ObservableListWrapper;
import is.CRCAnalyzerApp;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dapqa on 21.03.2016.
 */
public class MainFormController {

    public Label maxConflictCount3;
    public Label maxConflictCRC3;
    public Label maxConflictCount2;
    public Label maxConflictCRC2;
    public Label maxConflictCount1;
    public Label maxConflictCRC1;
    public Label message;
    public Label maxConflictCRC0;
    public Button calcButton;
    public Label maxConflictCount0;
    public TextField inputFilePath;
    public BarChart crc4Chart;
    public BarChart crc5Chart;
    public BarChart crc8Chart;
    public BarChart crc12Chart;
    public Pane waitPane;

    private Label[] maxConflictingCRCs;
    private Label[] maxConflictingCounts;
    private BarChart[] crcCharts;

    private Stage wordListStage;
    private WordListFormController wordListFormController;

    private CRCAnalyzerApp crcAnalyzerApp;
    private Stage stage;
    private CRCAnalyzerApp.CalculationResult[] lastResult;

    private FileChooser fileChooser = new FileChooser();

    @FXML
    public void initialize() throws IOException {
        createWordListStage();
        fillElementArrays();
        configureCharts();

        calcButton.setDisable(true);

        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Текстовые файлы", "*.txt"));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Все файлы", "*.*"));
    }

    private void createWordListStage() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResourceAsStream("/fxml/WordListForm.fxml"));

        wordListStage = new Stage();
        wordListStage.setScene(new Scene(root));
        wordListStage.setResizable(false);
        wordListStage.sizeToScene();
        wordListStage.initModality(Modality.APPLICATION_MODAL);

        wordListFormController = loader.getController();
        wordListFormController.setStage(wordListStage);
    }

    private void fillElementArrays() {
        maxConflictingCRCs = new Label[] {
                maxConflictCRC0, maxConflictCRC1, maxConflictCRC2, maxConflictCRC3
        };
        maxConflictingCounts = new Label[] {
                maxConflictCount0, maxConflictCount1, maxConflictCount2, maxConflictCount3
        };
        crcCharts = new BarChart[] {
                crc4Chart, crc5Chart, crc8Chart, crc12Chart
        };
    }

    private void configureCharts() {
        for (BarChart crcChart : crcCharts) {
            crcChart.setBackground(Background.EMPTY);
            crcChart.setVerticalGridLinesVisible(false);
            crcChart.setHorizontalGridLinesVisible(false);
            crcChart.getXAxis().setTickLabelsVisible(false);
            crcChart.getXAxis().setOpacity(0.);
            crcChart.setAnimated(false);
        }

    }

    public void chooseInputFile(ActionEvent actionEvent) {
        File inputFile = fileChooser.showOpenDialog(stage);
        if (inputFile != null) {
            try {
                inputFilePath.setText(inputFile.getCanonicalPath());
            } catch (IOException ignore) {}
        } else {
            inputFilePath.setText("");
        }

        crcAnalyzerApp.setInputFile(inputFile);
        calcButton.setDisable(inputFile == null);
    }

    public void calculate(ActionEvent actionEvent) {
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                Platform.runLater(() -> waitPane.setVisible(true));

                try {
                    lastResult = crcAnalyzerApp.calculate();
                    Platform.runLater(() -> {
                        updateResults();
                        drawCharts();
                    });
                } catch (IOException e) {
                    showError("Ошибка ввода-вывода", e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                    showError("Непредвиденная ошибка", e.getMessage());
                }

                Platform.runLater(() -> waitPane.setVisible(false));
                return null;
            }
        };

        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    private void updateResults() {
        if (lastResult == null) return;

        for (int i = 0; i < lastResult.length; i++) {
            maxConflictingCRCs[i].setText("0x" + Integer.toHexString(lastResult[i].maxConflictingCRC));
            maxConflictingCounts[i].setText(Integer.toString(lastResult[i].wordsWithMaxConflictingCRC.length));
        }

        message.setText("Результаты анализа файла " + inputFilePath.getText());
    }

    @SuppressWarnings("unchecked")
    private void drawCharts() {
        if (lastResult == null) return;

        for (int i = 0; i < lastResult.length; i++) {
            List<XYChart.Data> countList = new ArrayList<>();
            for (int j = 0; j < lastResult[i].counts.length; j++) {
                countList.add(new XYChart.Data("0x" + Integer.toHexString(j), lastResult[i].counts[j]));
            }

            XYChart.Series series = new XYChart.Series(new ObservableListWrapper<>(countList));

            crcCharts[i].getData().clear();
            crcCharts[i].getData().addAll(series);
        }
    }

    public void showMaxConflictingWordsList0(Event event) {
        if (lastResult != null) {
            showWordsList("CRC-4", lastResult[0].wordsWithMaxConflictingCRC);
        }
    }

    public void showMaxConflictingWordsList1(Event event) {
        if (lastResult != null) {
            showWordsList("CRC-5", lastResult[1].wordsWithMaxConflictingCRC);
        }
    }

    public void showMaxConflictingWordsList2(Event event) {
        if (lastResult != null) {
            showWordsList("CRC-8", lastResult[2].wordsWithMaxConflictingCRC);
        }
    }

    public void showMaxConflictingWordsList3(Event event) {
        if (lastResult != null) {
            showWordsList("CRC-12", lastResult[3].wordsWithMaxConflictingCRC);
        }
    }

    private void showWordsList(String title, String[] words) {
        wordListStage.setTitle(title);
        wordListFormController.setWords(words);
        wordListStage.show();
    }


    private void showError(String headerText, String contentText) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText(headerText);
            alert.setContentText(contentText);

            alert.showAndWait();
        });
    }

    public void setCrcAnalyzerApp(CRCAnalyzerApp crcAnalyzerApp) {
        this.crcAnalyzerApp = crcAnalyzerApp;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

}
